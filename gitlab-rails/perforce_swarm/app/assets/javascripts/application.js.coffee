#= require perforce_swarm/turbolinks
#= require ../../../../app/assets/javascripts/application
#= require js-routes
#= require jstree
#= require_self
#= require perforce_swarm/storage
#= require perforce_swarm/navbar
#= require perforce_swarm/p4_tree
#= require perforce_swarm/dashboard
#= require perforce_swarm/projects
#= require perforce_swarm/reenable_helix_mirroring

# define the swarm global variable
@swarm = {}
