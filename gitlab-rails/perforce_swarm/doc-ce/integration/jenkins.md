# Jenkins CI integration

GitSwarm 2015.4, and earlier releases, could use the [GitLab Hook
Plugin](https://wiki.jenkins-ci.org/display/JENKINS/GitLab+Hook+Plugin).
That plugin has since been deprecated, and it is recommended that users
use the newer [GitLab
Plugin](https://wiki.jenkins-ci.org/display/JENKINS/GitLab+Plugin).

Installation and configuration instructions are available on the [GitLab
Plugin repo on GitHub](https://github.com/jenkinsci/gitlab-plugin).
