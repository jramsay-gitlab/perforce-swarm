# Add a welcome message to the sign-in page

It is possible to add a markdown-formatted welcome message to your GitSwarm
sign-in page. Users of GitSwarm Enterprise Edition should use the branded
login page feature instead.

The welcome message can be set/changed in the Admin UI: Admin area >
Settings, in the `Sign in text` field.
