# GitLab Pages

> **Note:** The GitLab Pages feature is currently not enabled, and GitSwarm
> EE 2016.1 does not present any of this feature's documentation.
>
> GitLab Pages was introduced in GitLab EE 8.3 (upon which GitSwarm EE
> 2016.1 is based), and was notably improved through GitLab EE 8.5. The
> GitSwarm EE team is working hard to maintain feature parity, and we
> should be able to fully include/support GitLab Pages in a future release
> (hopefully, within 2016.2).
