# GitSwarm operations

- [Configuring GitSwarm EE for large pushes](large_push_config.md)
- [Sidekiq MemoryKiller](sidekiq_memory_killer.md)
- [Understanding Unicorn and unicorn-worker-killer](unicorn.md)
